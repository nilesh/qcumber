# This config file controls the apptests for qcumber

# Its toplevel consists of
# references, seeds, default_output_files, download_minikraken and test_runs
# verbosity
# TODO: download_minikraken

# short reminder: - denotes the start of a list element
#                   still part of the element
#                   again.. first entry of the list
#                 - second entry of the list
#   also a list: [1, 2, 3, 4, 5, 6]

# references:    # Reference sequences used mainly for simulating reads
#   <name_ref>:  # Name used to reference the sequence within the document
#       descriptive_name: <description>
#          # As it states (Can be an empty string '')
#       url: <url> (required and should be valid)
#          # Webaddress from which to download data if it doesn't exist
#          # yet. Multi-line strings that are supposed to be one big one
#          # can be enclosed with quotes "", but each line
#          # needs to end with a forward slash, to prevent the insertion
#          # of spaces.
# remote_reads: # Remote fastq data
#   <data_ref>: # Name used to reference data
#       descriptive_name: <description_dataset>
#         - desriptive_name: <desription_file>
#           url: <url>
# seeds: [] list of seeds required to run the simulators (non optional)
#
# verbosity: <int> (optional, default: 1)
#   # verbosity level of qcumber run (does not affect log)
#   # level 0: no qcumber output
#   # level 1: only progress and errors
#   # level 2: all rule, job and progress information
# default_output_files:
#   # List of output files that need monitoring
#   # OutputFile List Entry:
#   # Files are hashed and compared to a golden standard or alternatively
#   # checked by looking at the file size
#
#   - file: <path>  (required)
#       # Filepath referenced to QCResults
#       # QCResults/_data/summary.json is _data/summary.json
#       # Unix- path pattern extension works (*[]?) and will
#       # be resolved as multiple files that are handled with the same
#       # options active.
#       # The test script generates pretty unwieldy file name prefixes.
#       # One does not need to anticipate thoose and can simply elect
#       # to use {{Prefix}} in the filepath, which will be replaced by the
#       # Prefix.
#
#     expected_num_files: <num> (optional, default: 1)
#       # Numbers of files to expect after resolving name and pattern
#       # extension. Causes WARNING, if more files than expected
#       # and ERROR if less than expected are observed
#
#     regex: 'regex string' (optional, default: '')
#       # A perl regex that will be used to pre-process the file prior
#       # to hashing it. Removing timestamps, temporary directory names and
#       # machine local variables are its main uses.
#       # 's/"Date": "[-0-9]*",//g' removes a date entry from a .json
#       # 's/tmp\/[^ ]*\///g' removes tmp/randomized_string/ from a file
#       # Sadly, Using multiple regexes currently requires stringing them
#       # together. E.g.: 's/("Date": "[-0-9]*",)|(tmp\/[^ ]*\/)//g'
#
#     use_file_size: <boolean> (optional, default: False)
#       # circumvents hashing and triggers comparison by file size
#
#     file_size_cutoff: <float> (optional, default: .95)
#       # Bound for file being close enough to the size of the standard
#       # Per default checks if the smaller file is at least 95% of the
#       # bigger one in size.
#       # If set to 1, needs an exact matching.
#       # Caution: File systems are weird and this might not work out well
#
# test_runs:   # list of test runs
#   # A test run
#   - threads: <num> (optional, default: 4)
#       # Threads Qcumber should use
#
#     trimbetter: <bool> (optional, default: False)
#       # Should the run use trimbetter
#
#     special_output_files: <list> (optional, default: [])
#       # Same as default output file list, but takes precedence
#       # over said list in case of a conflict (see: default_output_files)
#
#     kraken: <bool> (optional, default: False)
#       # Use kraken database to check for possible contamination
#       # of read data
#       #
#       # If QCumber/config/config.txt is not setup to find a suitable
#       # kraken database one can be pointed out in the additional opts.
#       # TODO:
#       # If none is provided and the optional parameter download_minikraken
#       # is set, minikraken will be downloaded (4 GB).
#
#     additional_opts: <list> (optional, default: [])
#       # additional options for the qcumber run
#       # Note: Options are not allowed to contain white spaces
#       # ['--kraken_db Path/to/kraken/database.kdb', ...] is not valid
#       # ['--kraken_db', 'Path/to/kraken/database.kdb', ...] is valid
#       # ['-dPath/to/kraken/database.kdb', ...] is also valid
#
#     simulators: <list> (optional, if real data present, default: [])
#     # Simulator entry
#       - name: <name of simulator> (required)
#           # currently only dwgsim implemented
#
#         reference: <name of reference> (required)
#           # must match a name assigned in the references section of this
#           # config
#
#         opt: <list> (required)
#           # list of options for start of read simulator
#           # Note: Options are not allowed to have white space
#           # ['-1 140', ...] is not allowed
#           # it must be either ['-1140', ...] or ['-1', '140', ...]
#         type: <type>  (optional, default: 'read') #  read | variants
#           # information used to manage multiple simulators, but currently
#           # unused.
#
#         qcumber_input: <bool> (optional, default: True)
#           # flag signaling that the simulation is used for the input
#           # parameter of qcumber. Causes name cleanup.
#
#     ## Here be Dragons ##
#     overwrite_standard: <bool> (optional, default: False)
#       # Overwrites the gold standard entry for the run.
#       # Usefull if you are sure that the previous entry was wrong.
#       # A tool for fixing rules and accidental activation should be
#       # avoided at all cost. This is why overwrites are only triggered
#       # if the ./test_qcumber is called with the --trigger_overwrite
#       # parameter.
#       #
#       # Use Case: You are working on a new file rule that requires
#       #   cleanup before hashing.
#       #   - You run test_qcumber and it generates new
#       #     hashes for the newly tracked files.
#       #   - The second run yields bad hashes for the new files.
#       #     There might be a timestamp, temporary directory, username that
#       #     needs to be removed prior to hashing
#       #   - You adjust the regex replace and set the overwrite_standard
#       #     option for the run to True.
#       #   - run test_qcumber.py --trigger_overwrite
#       #   - remove the flag and test again
#

---
references:
    e_coli:
          descriptive_name: Escherichia coli str. K-12 substr. MG1655
          url:
            "ftp://ftp.ncbi.nlm.nih.gov/genomes/\
             all/GCF/000/005/845/GCF_000005845.2_ASM584v2/\
             GCF_000005845.2_ASM584v2_genomic.fna.gz"

    influenza_A:
          descriptive_name: Influenza A virus (A/New York/392/2004(H3N2))
          url:
            "ftp://ftp.ncbi.nlm.nih.gov/genomes/\
             all/GCF/000/865/085/\
             GCF_000865085.1_ViralMultiSegProj15622/\
             GCF_000865085.1_ViralMultiSegProj15622_genomic.fna.gz"

remote_reads: # Remote fastq data
    SRR390728: # Name used to reference data
          descriptive_name: >
            Illumina Genome Analyzer IIx paired end sequencing;
            RNA-Seq (polyA+) analysis of DLBCL cell line HS0798
          files:
            - descriptive_name: "Readends (1/2) of paired end read data"
              url:
                "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR390/SRR390728/\
                 SRR390728_1.fastq.gz"
            - descriptive_name: "Readends (2/2) of paired end read data"
              url:
                "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/SRR390/SRR390728/\
                 SRR390728_2.fastq.gz"

verbosity: 1 # (0/1/2)
seeds: [42, 4242, 1337, 20019812, 799821123]
default_output_files:

    - file: _data/summary_new.json
      use_file_size: False

    - file: _data/summary.json
      regex: 's/("Date": "[-0-9]*",)|(tmp\/[^ ]*\/)|(\/home[^ ]*.fa)//g'
      use_file_size: False

test_runs: # List of runs
    - kraken: False
      trimbetter: False
      threads: 2
      additional_opts: []
      default_to_simref: False
      special_output_files:

        - file: _logfiles/*.trimmomatic.log
          regex: 's/(tmp\/[^ ]*\/)|(\/home[^ ]*.fa)//g'
          use_file_size: False

        - file: FastQC/trimmed/*
          use_file_size: True
          expected_num_files: 2

      simulators:
        - name: dwgsim
          type: read
          qcumber_input: True
          reference: e_coli
          opt: ["-1120", "-2120", -e0.0005-0.001, -E0.0005-0.001, -N1000]

    - kraken: False
      trimbetter: True
      trimbetter_mode: mapping
      threads: 2
      additional_opts: []
      default_to_simref: False
      special_output_files:

        - file: _logfiles/*.trimmomatic.log
          regex: 's/(tmp\/[^ ]*\/)|(\/home[^ ]*.fa)//g'
          use_file_size: False

        - file: FastQC/trimmed/*
          use_file_size: True
          expected_num_files: 2

      simulators:
        - name: dwgsim
          type: read
          qcumber_input: True
          reference: e_coli
          opt: ["-1120", "-2120", -e0.0005-0.001, -E0.0005-0.001, -N1000]
#      local_real_data:
#       - input_files:
#           - file: ./data/WierdSpec
#             cutoff: 200
#        - project_name: Morpheus
#          input_files:
#            - file: the/chosen/path/*
#    - kraken: False
#      trimbetter: False
#      threads: 4
#      default_to_simref: False
#      additional_opts: []
#      special_output_files:
#        - file: _logfiles/{{Prefix}}*.trimmomatic.log
#          regex: 's/(tmp\/[^ ]*\/)|(\/home[^ ]*.fa)//g'
#          use_file_size: False
#      simulators:
#        - name: dwgsim
#          type: read
#          qcumber_input: True
#          reference: e_coli
#          opt: ["-1120", "-2110", -e0.0005-0.002, -E0.0005-0.001, -N100000]
#      overwrite_standard: False

    - kraken: False
      trimbetter: False
      threads: 4
      default_to_simref: True
      additional_opts: ['-S']
      special_output_files:
        - file: _logfiles/{{Prefix}}*.trimmomatic.log
          regex: 's/(tmp\/[^ ]*\/)|(\/home[^ ]*.fa)//g'
          use_file_size: False
      simulators:
        - name: dwgsim
          type: read
          qcumber_input: True
          reference: e_coli
          opt: ["-1120", "-2115", -e0.0005-0.002, -E0.0005-0.001, -N100000]
      overwrite_standard: False
